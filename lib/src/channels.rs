#[derive(Debug, Clone)]

pub struct Channels<T: Clone + Copy + Default> {
    left: Vec<T>,
    right: Vec<T>,
}

impl<T: Clone + Copy + Default> Channels<T> {
    #[allow(clippy::new_without_default)]
    pub fn new(channel_len: usize) -> Self {
        Channels {
            left: vec![T::default(); channel_len],
            right: vec![T::default(); channel_len],
        }
    }

    pub fn left(&self) -> &[T] {
        &self.left
    }

    pub fn right(&self) -> &[T] {
        &self.right
    }

    pub fn left_mut(&mut self) -> &mut [T] {
        &mut self.left
    }

    pub fn right_mut(&mut self) -> &mut [T] {
        &mut self.right
    }
}
